
\documentclass{article} 
\oddsidemargin=-5mm
\evensidemargin=-5mm
\marginparwidth=.08in 
\marginparsep=.01in
\marginparpush=5pt
\topmargin=-15mm
\headheight=12pt
\headsep=25pt
\footskip=30pt
\textheight=25cm
\textwidth=17cm
\columnsep=2mm
\columnseprule=1pt
\parindent=15pt
\parskip=2pt


\usepackage[utf8]{inputenc} % LaTeX source encoded as UTF-8
\usepackage{graphicx} %graphics files inclusion
\usepackage{caption}
\usepackage{subcaption}
\usepackage{pdfpages}
\usepackage{amsmath}
\usepackage{fancyvrb}
\pagestyle{myheadings}
\pagenumbering{arabic}     % zapne obyčejné číslování
\setcounter{page}{1}  % nastaví čítač stránek znovu od jedné

\begin{document}
\begin{center}
\bf Semestrální projekt MI-PDP 2018/2019\\[5mm]
    Paralelní algoritmus pro řešení problému pokrývání plochy L dlaždicemi\\[5mm] 
       Vojtěch Polcar\\[2mm]
magisterské studium, FIT ČVUT, Thákurova 9, 160 00 Praha 6\\[2mm]
\end{center}

\section{Definice problému a popis sekvenčního algoritmu}

Zadaným problémem je pro mřížku R se zakázanými políčky danými polem D najít pokrytí dlaždicemi L3 a L4 (viz obrázek) s maximální cenou. Dlaždice lze pravoúhle otáčet a obracet (viz obrázek).

\begin{center}
\includegraphics[width=0.3\textwidth]{tiles.png}
\end{center}

\noindent
\textbf{Cena} pokrytí, ve kterém zbylo q nepokrytých nezakázaných políček se počítá dle vzorce:

\begin{center}
\begin{BVerbatim}
2*počet dlaždic tvaru L3 + 3*počet dlaždic tvaru L4 - 6*q
\end{BVerbatim}
\end{center}

\subsection*{Vstupní data}
Vstupem pro řešení problému jsou následující data:
\begin{itemize}
	\item \textbf{m, n} - rozměry obdélníkové mřížky R[1..m,1..n], skládající se z m x n políček,
	\item \textbf{k $<$ m*n} - počet zakázaných políček v mřížce R,
	\item \textbf{D[1..k]} - pole souřadnic k zakázaných políček náhodně rozmístěných v mřížce R.
\end{itemize}

\subsection*{Výstup algoritmu}
\begin{itemize}
	\item Číslo udávající maximální cenu pokrytí a počty dlaždic L3 a L4 a počet nepokrytých políček a jejich souřadnice.
	\item Popis pokrytí mřížky R, např. (m x n)-maticí identifikátorů, kde každá dlaždice je jednoznačně určena políčky s unikátním ID$>=$1, nepokrytá políčka jsou prázdná a zakázaná jsou reprezentovaná znakem 'Z'.
\end{itemize}

\subsection*{Sekvenční algoritmus}
Sekvenční algoritmus se skládá z několik částí a je možné přistoupit k němu buď iterativně nebo rekurzivně. V mém řešení jsem se rozhodl implementovat rekurzivní verzi. Vybral jsem si programovací jazyk C++ a program rozdělil na několik tříd, které mají přidělené úkoly:
\begin{description}
	\item[cconfiguration] Třída, která reprezentuje samotnou mřížku a z ní počítá cenu pokrytí.
	\item[csolver] Třída, která se stará o samotné řešení problému - provádí rekurzivní volání, porovnání nalezených cen a v paralelní fázi i paralelní výpočty.
\end{description}

\noindent
Sekvenční algoritmus je založený na prohledávání stavového prostoru do hloubky s řadou vylepšení a ořezávání, které zrychlují výpočet. Třída \verb|csolver| si nejprve načte mřížku se zakázanými políčky. V prvním kroku se nejprve vygeneruje nějaký počet řešení (závislý na počtu vláken/procesů, které provádí výpočet).
	\par
Po vygenerování startovních řešení se z těchto stavů pouští prohledávání do hloubky. To je řešeno in-place a nedochází tak ke zpomalování kvůli kopírování. Ořezávání provádím pomocí přiložené funkce pro výpočet maximální možné dosažitelné ceny z daného počtu volných políček. Maximální možná dosažitelná cena se porovná s aktuálně nejlepší nalezenou cenou pokrytí a v případě, že je menší nebo rovna, dojde k navrácení z dané větve zpět, protože již dále nelze nalézt lepší řešení.

\noindent
Pro testování jsem využil tří různých vstupních mřížek, ve kterých byla políčka, která se nedala nikdy zaplnit. Vstupy jsou uloženy na gitlabu a součástí archivu na progtestu. Dobu sekvenčního výpočtu na vybraných instancích můžeme vidět v tabulce \ref{table_time}.
\begin{table}\centering
	\caption{Časy výpočtů sekvenčního řešení nad danými vstupy}\label{table_time}
	\begin{tabular}{|c|c|c|}\hline
		\textbf{Instance}	& \textbf{Čas v milisekundách} & \textbf{MM:SS}\tabularnewline \hline \hline
		A & 194180 & 3:15 \tabularnewline \hline
		B & 564334 & 9:24 \tabularnewline \hline
		C & 813828 & 13:41 \tabularnewline \hline
	\end{tabular}
\end{table}



\section{OpenMP - task paralelismus}

Pro použití paralelismu na úrovni tasků bylo potřeba implementovat funkce knihovny OpenMP. Postup programu zůstává prakticky stejný a je změněn pouze v paralelním výpočtu startovních řešení. Task paralelismus funguje tak, že si na začátku definuji počet vláken, které budou provádět paralelní výpočet. Následně si vygeneruji počet výchozích řešení, která budou vlákna řešit. 
	\par
Počet výchozích řešení je generován podle počtu vláken, který je vynásoben konstantou - v mém případě 24. Řešení jsou uložena ve frontě, ze které si je jednotlivá vlákna vybírají. Výběr řešení z fronty je obalený kritickou sekcí, stejně tak i zapisování nalezeného výsledku. 
	\par
Při rozšiřování sekvenčního řešení bylo potřeba v programu přidat kompletně novou třídu pro reprezentaci mřížky, aby s ní bylo možné dále pracovat a pracovat s ní paralelně.

\section{OpenMP - datový paralelismus}

Datový paralelismus je odlišný druh přístupu k paralelnímu rozdělení práce. Z vygenerovaných řešení (kterých je nyní nutné mít minimálně stejně jako vláken) se vytvoří pole, přes které se v paralelním for cyklu provádí výpočty. Počet úloh je rovnoměrně rozdělen podle počtu výpočetních vláken - každé vlákno dostane přiřazenou určitou část pole, kterou vykoná.
	\par
Postup je velmi podobný jako u task paralelismu. Nejprve vygeneruji výchozí řešení (jejich počet je opět závisí na počtu vláken a násobícímu koeficientu -- 24) a následně nad polem těchto řešení zavolám paralelní for cyklus. Jednotlivá řešení jsou prováděna sekvenčně.
	\par
Pro přidání datového paralelismu bylo potřeba pouze přepsat původní frontu s řešeními na oboustrannou frontu.


\section{Popis paralelního algoritmu a jeho implementace v MPI}

Z dvou výše uvedených způsobů jsem si vybral datový paralelismus. Nad ten je pro systémy s distribuovanou pamětí zapracován pomocí knihovny MPI Master-slave algoritmus. Ten se dělí na dvě části.

\subsection*{Master}
Master řídí všechny pracující procesy a přiděluje jim práci. V mé variantě řešení sám nikdy nepracuje a pouze na práci dohlíží. Postup je programu v Master procesu je následující:

\begin{enumerate}
	\item Nejdříve se provede inicializace a načtení řešení ze vstupního souboru.
	\item Poté je vygenerováno p * k startovních řešení. (p je počet procesorů, k je konstanta)
	\item V dalším kroku proběhne přidělení práce z vygenerovaných startovních řešení všem slave procesům.
	\item Poté ve smyčce čekáme na odpovědi s řešeními od slave procesů. Po přijmutí odpovědi se provede porovnání s nejlepším dosud nalezených řešením a jeho případné nahrazení.
	\item Pokud ještě zbývá nějaká práce ve frontě, je slave procesu opět odeslána, jinak je odesláno oznámení o ukončení práce.
\end{enumerate}

\subsection*{Slave}
Slave proces pouze přijímá konfigurace k řešení od Master procesu.

\begin{enumerate}
	\item Slave čeká na zaslání práce od Master procesu.
	\item Po obdržení konfigurace se z ní nejdříve vygeneruje dalších t * k počet startovních řešení. (t je počet vláken, k je konstanta)
	\item Poté vyřeší tato vygenerovaná řešení pomocí datového paralelismu.
	\item Nejlepší nalezené řešení je odesíláno Master procesu a poté slave čeká na další práci.
\end{enumerate}

\noindent
Pro MPI komunikaci využívám blokujících volání. Kromě zadané konfigurace k řešení odesílám slave procesům k řešení také aktuálně nejlepší dosažené řešení, podle kterého poté mohou více ořezávat.

\section{Naměřené výsledky a vyhodnocení}
K měření jsem využíval vždy jeden Master proces a dva Slave procesy s využitím až 24 vláken. Z výsledků je patrné, že rychlost výpočtu je silně závislá na datové instanci. Dalším faktem by mohlo být to, že je stavový prostor nerovnoměrně rozložený. Při měření se mi často stávalo, že jeden slave proces doběhl o dost dříve než druhý. (varianta task paralelismu by mohla být řešení)
	\par
Pro instanci A, která se při sekvenčním řešení počítá 191 sekund, nedochází ke zrychlení do té doby než je pro výpočet použito 20 vláken. Pak dojde k enormnímu zrychlení -- výpočet trvá pouze 14 sekund. V tu chvíli dojde pravděpodobně k rozšíření stavového prostoru tak, že je velmi rychle nalezeno nejlepší řešení.
	\par
V instanci B naopak dojde k největšímu zrychlení pro dvě vlákna. Od té doby jsou časy násobně horší (viz. tabulka \ref{table_measuring}). To je pravděpodobně způsobeno konstantou a velkým množstvím vygenerovaných pod-řešení. Tato instance ve finální mřížce obsahuje vždy jedno volné políčko, což znamená, že se prohledává kompletně celý stavový prostor.
	\par
Instance C se chová podobně jako instance A. Všechny výsledky měření jsou graficky zobrazeny v grafu níže.

Program s algoritmem by se dal rozhodně vylepšit a patrně by to mohlo vést k lepším a výsledkům, ač by zde stále zůstávala závislost na tom, na jaké instanci bychom program spustili.

\begin{center}
	\includegraphics[width=1\textwidth]{plot.png}\label{plot_of_results}
\end{center}

\begin{table}\centering
	\caption{Časy výpočtů v ms na instancích A, B, C}\label{table_measuring}
	\begin{tabular}{|c|c|c|c|}\hline
		\textbf{Počet vláken} & \textbf{A}	& \textbf{B} & \textbf{C}\tabularnewline \hline \hline
		1 & 194180 & 564344 & 813828 \tabularnewline \hline
		2 & 196301 & 97658 & 569284 \tabularnewline \hline
		4 & 201055 & 105888 & 424802 \tabularnewline \hline
		6 & 206387 & 217563 & 378029 \tabularnewline \hline
		8 & 80132 & 222228 & 384002 \tabularnewline \hline
		12 & 88805 & 228921 & 388817 \tabularnewline \hline
	    14 & 89693 & 233465 & 142985 \tabularnewline \hline
		16 & 95832 & 240450 & 136382 \tabularnewline \hline
		20 & 14430 & 240186 & 121739 \tabularnewline \hline
		24 & 14782 & 253290 & 123094 \tabularnewline \hline
	\end{tabular}
\end{table}


\section{Závěr}

Celkově hodnotím semestrální práci za velmi zajímavou a přínosnou. Samotné programování nebylo náročné (obsahující nějaké chytáky) a byly vidět zajímavé a očekávané výsledky. Paralelní výpočet zlepšil původní výsledky sekvenčního řešení i když hodně záleží také na vstupní instanci. Rozhodně mě práce s paralelismem v tomto předmětu bavila mnohem více než v bakalářském kurzu BI-OSY.
	\par
K samotné implementaci bych v případě znalosti výsledného programu a knihoven přistupoval jinak. Při přechodu na OpenMP a MPI jsem vždy musel dělat úpravy v samotných třídách a rozšiřovat je o nové funkce, což se projevilo na jejich výsledné přehlednosti.
	\par
Rozhodně bych také upravil MPI implementaci, která se na serveru star ladila poněkud hůře. Za zvážení by jistě stálo i lepší šíření informace o nejlepším nalezeném řešení (v MPI části).

\end{document}
