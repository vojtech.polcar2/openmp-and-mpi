#ifndef PDP_SEMESTRAL_CONFIGURATION_H
#define PDP_SEMESTRAL_CONFIGURATION_H

using namespace std;

#include <iostream>
#include <iomanip>
#include <vector>

/* Class which holds the state of tree in solutions. */
class CConfiguration{
public:
    CConfiguration(){};
    CConfiguration( vector<vector<int>> matrix, int l3Cnt, int l4Cnt, int id, int emptyTiles, int impossibleTiles );
    ~CConfiguration(){};
    vector<vector<int>> field;
    int l3Cnt;
    int l4Cnt;
    int emptyTiles;
    int imposibleTiles;
    int id;
    vector<vector<int>> getField();
    void fillBufferToSend( int * buffer);
    void loadDataFromBuffer( int * buffer, int N, int M );

    int bufferSize();

    friend ostream & operator<<(ostream & os , const CConfiguration & obj);
};

#endif //PDP_SEMESTRAL_CONFIGURATION_H
