//
// Created by trapper on 3.3.19.
//

using namespace std;

#include "csolver.h"

#define NUM_THREADS 2
#define NUM_CONFIGURATIONS 24

CSolver::CSolver(const CSolver &csolver) {
    this->N = csolver.N;
    this->M = csolver.M;
    this->bestPrice = csolver.bestPrice;
    this->bestl3Cnt = csolver.bestl3Cnt;
    this->bestl4Cnt = csolver.bestl4Cnt;
    this->bestEmptyTiles = csolver.bestEmptyTiles;
    this->Z = csolver.Z;
    for (int i = 0; i < this->N; i++){
        // construct a vector of int
        vector<int> v;
        for (int j = 0; j < this->M; j++)
            v.push_back(csolver.field[i][j]);

        this->field.push_back(v);
        this->bestSolution.push_back(v);
    }
    solutionPool;
}

void CSolver::loadSolution( int p ) {
    int n = 0, m = 0;
    cin >> n >> m;
    this->N = m;
    this->M = n;
    this->NUM_PROCS = p;

    for (int i = 0; i < this->N; i++){
        // construct a vector of int
        vector<int> v;
        for (int j = 0; j < this->M; j++)
            v.push_back(0);

        this->field.push_back(v);
        this->bestSolution.push_back(v);
    }
    this->bestPrice = 0;
    this->bestl3Cnt = 0;
    this->bestl4Cnt = 0;
    this->bestEmptyTiles = 0;
    this->Z = 0;
    solutionPool;

    forbiddenTiles();
}

/* Print operator */
ostream & operator<<(ostream & os, const CSolver & obj){
    for( vector<int> row : obj.bestSolution){
        for( int value : row ){
            os << setw(2) << value << " ";
        }
        os << endl;
    }
    return os;
}

int CSolver::getBestPrice() {
    return this->bestPrice;
}

int CSolver::setN( int N ){
    this->N = N;
}

int CSolver::setM( int M ){
    this->M = M;
}

int CSolver::setBestPrice( int bestPrice ){
    this->bestPrice = bestPrice;
}

int CSolver::getN() {
    return this->N;
}

int CSolver::getM() {
    return this->M;
}

void CSolver::loadDataFromBuffer( int * buffer ){
    this->bestPrice = buffer[0];
    this->bestEmptyTiles = buffer[1];
    this->bestl4Cnt = buffer[2];
    this->bestl3Cnt = buffer[3];
    solutionPool;

    int k = 10;
    this->field.clear();
    this->bestSolution.clear();
    for (int i = 0; i < this->N; i++){
        // construct a vector of int
        vector<int> v;
        for (int j = 0; j < this->M; j++) {
            v.push_back(buffer[k]);
            k++;
        }


        this->field.push_back(v);
        this->bestSolution.push_back(v);
    }
}

void CSolver::fillDataToBuffer( int * buffer ){
    buffer[0] = this->bestPrice;
    buffer[1] = this->bestEmptyTiles;
    buffer[2] = this->bestl4Cnt;
    buffer[3] = this->bestl3Cnt;
    int k = 4;
    for(int i = 0; i < bestSolution.size(); i++){
        for( int j = 0; j < bestSolution[i].size(); j++ ) {
            buffer[k] = bestSolution[i][j];
            k++;
        }
    }
}

void CSolver::fillBufferWithBestValues( int * buffer ){
    buffer[0] = this->bestPrice;
    buffer[1] = this->bestEmptyTiles;
    buffer[2] = this->bestl4Cnt;
    buffer[3] = this->bestl3Cnt;
}


/* Returns max price of unsolved tiles. */
int CSolver::evalPol( int number ){
    int mod=number%5;
    if (mod==0) return (number/5)*3;
    if (mod==1) return ((number-16)/5)*3+4*2;
    if (mod==2) return ((number-12)/5)*3+3*2;
    if (mod==3) return ((number-8)/5)*3+2*2;
    if (mod==4) return ((number-4)/5)*3+2;
}

/* This function load positions of forbidden tiles and place them in the field. */
void CSolver::forbiddenTiles() {
    int z = 0, x = 0, y = 0;
    cin >> z;
    this->Z = z;

    for( int i = 0; i < z; i++ ){
        cin >> x >> y;
        this->field[x][y] = -1;
        this->bestSolution[x][y] = -1;
    }

    this->bestEmptyTiles = this->N*this->M - this->Z;
}

/* Function which with given parameters check if it's possible to place certain L tile. */
bool CSolver::checkTiles( int x, int y, int shape, CConfiguration & configuration ) {
    switch ( shape ){
        case 0:
            if( ( (x + 3) > N - 1) || ( (y + 1) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+2][y] != 0 || configuration.field[x+3][y] != 0 || configuration.field[x][y+1] != 0 )
                return false;
            else return true;
        case 1:
            if( ( (x + 1) > N - 1) || ( (y + 3) > M - 1))
                return false;
            else if( configuration.field[x+1][y+3] != 0 || configuration.field[x][y+1] != 0 || configuration.field[x][y+2] != 0 || configuration.field[x][y+3] != 0 )
                return false;
            else return true;
        case 2:
            if( ( (x + 3) > N - 1) || ( (y - 1) < 0))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+2][y] != 0 || configuration.field[x+3][y] != 0 || configuration.field[x+3][y-1] != 0 )
                return false;
            else return true;
        case 3:
            if( ( (x + 1) > N - 1) || ( (y + 3) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+1][y+1] != 0 || configuration.field[x+1][y+2] != 0 || configuration.field[x+1][y+3] != 0 )
                return false;
            else return true;
        case 4:
            if( ( (x + 3) > N - 1) || ( (y + 1) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+2][y] != 0 || configuration.field[x+3][y] != 0 || configuration.field[x+3][y+1] != 0 )
                return false;
            else return true;
        case 5:
            if( ( (x + 1) > N - 1) || ( (y + 3) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x][y+1] != 0 || configuration.field[x][y+2] != 0 || configuration.field[x][y+3] != 0 )
                return false;
            else return true;
        case 6:
            if( ( (x + 3) > N - 1) || ( (y + 1) > M - 1))
                return false;
            else if( configuration.field[x+1][y+1] != 0 || configuration.field[x+2][y+1] != 0 || configuration.field[x+3][y+1] != 0 || configuration.field[x][y+1] != 0 )
                return false;
            else return true;
        case 7:
            if( ( (x - 1) < 0 ) || ( (y + 3) > M - 1))
                return false;
            else if( configuration.field[x][y+1] != 0 || configuration.field[x][y+2] != 0 || configuration.field[x][y+3] != 0 || configuration.field[x-1][y+3] != 0 )
                return false;
            else return true;
        case 8:
            if( ( (x + 2) > N - 1) || ( (y + 1) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+2][y] != 0 || configuration.field[x][y+1] != 0 )
                return false;
            else return true;
        case 9:
            if( ( (x + 1) > N - 1) || ( (y + 2) > M - 1))
                return false;
            else if( configuration.field[x+1][y+2] != 0 || configuration.field[x][y+1] != 0 || configuration.field[x][y+2] != 0 )
                return false;
            else return true;
        case 10:
            if( ( (x + 2) > N - 1) || ( (y - 1) < 0))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+2][y] != 0 || configuration.field[x+2][y-1] != 0 )
                return false;
            else return true;
        case 11:
            if( ( (x + 1) > N - 1) || ( (y + 2) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+1][y+1] != 0 || configuration.field[x+1][y+2] != 0 )
                return false;
            else return true;
        case 12:
            if( ( (x + 2) > N - 1) || ( (y + 1) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x+2][y] != 0 || configuration.field[x+2][y+1] != 0 )
                return false;
            else return true;
        case 13:
            if( ( (x + 1) > N - 1) || ( (y + 2) > M - 1))
                return false;
            else if( configuration.field[x+1][y] != 0 || configuration.field[x][y+1] != 0 || configuration.field[x][y+2] != 0 )
                return false;
            else return true;
        case 14:
            if( ( (x + 2) > N - 1) || ( (y + 1) > M - 1))
                return false;
            else if( configuration.field[x+1][y+1] != 0 || configuration.field[x+2][y+1] != 0 || configuration.field[x][y+1] != 0 )
                return false;
            else return true;
        case 15:
            if( ( (x - 1) < 0 ) || ( (y + 2) > M - 1))
                return false;
            else if( configuration.field[x][y+1] != 0 || configuration.field[x][y+2] != 0 || configuration.field[x-1][y+2] != 0 )
                return false;
            else return true;

        default:
            return false;
    }
}

/* Function which fill tiles with id of the L tile. */
void CSolver::fillTiles( int x, int y, int id, int shape, CConfiguration & configuration ) {
    switch ( shape ){
        case 0:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+2][y] = id;
            configuration.field[x+3][y] = id;
            configuration.field[x][y+1] = id;
            break;
        case 1:
            configuration.field[x][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x][y+2] = id;
            configuration.field[x][y+3] = id;
            configuration.field[x+1][y+3] = id;
            break;
        case 2:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+2][y] = id;
            configuration.field[x+3][y] = id;
            configuration.field[x+3][y-1] = id;
            break;
        case 3:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+1][y+1] = id;
            configuration.field[x+1][y+2] = id;
            configuration.field[x+1][y+3] = id;
            break;
        case 4:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+2][y] = id;
            configuration.field[x+3][y] = id;
            configuration.field[x+3][y+1] = id;
            break;
        case 5:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x][y+2] = id;
            configuration.field[x][y+3] = id;
            break;
        case 6:
            configuration.field[x][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x+1][y+1] = id;
            configuration.field[x+2][y+1] = id;
            configuration.field[x+3][y+1] = id;
            break;
        case 7:
            configuration.field[x][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x][y+2] = id;
            configuration.field[x][y+3] = id;
            configuration.field[x-1][y+3] = id;
            break;
        case 8:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+2][y] = id;
            configuration.field[x][y+1] = id;
            break;
        case 9:
            configuration.field[x][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x][y+2] = id;
            configuration.field[x+1][y+2] = id;
            break;
        case 10:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+2][y] = id;
            configuration.field[x+2][y-1] = id;
            break;
        case 11:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+1][y+1] = id;
            configuration.field[x+1][y+2] = id;
            break;
        case 12:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x+2][y] = id;
            configuration.field[x+2][y+1] = id;
            break;
        case 13:
            configuration.field[x][y] = id;
            configuration.field[x+1][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x][y+2] = id;
            break;
        case 14:
            configuration.field[x][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x+1][y+1] = id;
            configuration.field[x+2][y+1] = id;
            break;
        case 15:
            configuration.field[x][y] = id;
            configuration.field[x][y+1] = id;
            configuration.field[x][y+2] = id;
            configuration.field[x-1][y+2] = id;
            break;

        case 16:

            configuration.field[x][y] = -2;
            break;
    }
}

/* Function which clear tiles at given coordinates from filled id. */
void CSolver::restartTiles( int x, int y, int shape, CConfiguration & configuration  ) {
    switch ( shape ) {
        case 0:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 2][y] = 0;
            configuration.field[x + 3][y] = 0;
            configuration.field[x][y + 1] = 0;
            break;
        case 1:
            configuration.field[x][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x][y + 2] = 0;
            configuration.field[x][y + 3] = 0;
            configuration.field[x + 1][y + 3] = 0;
            break;
        case 2:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 2][y] = 0;
            configuration.field[x + 3][y] = 0;
            configuration.field[x + 3][y - 1] = 0;
            break;
        case 3:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 1][y + 1] = 0;
            configuration.field[x + 1][y + 2] = 0;
            configuration.field[x + 1][y + 3] = 0;
            break;
        case 4:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 2][y] = 0;
            configuration.field[x + 3][y] = 0;
            configuration.field[x + 3][y + 1] = 0;
            break;
        case 5:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x][y + 2] = 0;
            configuration.field[x][y + 3] = 0;
            break;
        case 6:
            configuration.field[x][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x + 1][y + 1] = 0;
            configuration.field[x + 2][y + 1] = 0;
            configuration.field[x + 3][y + 1] = 0;
            break;
        case 7:
            configuration.field[x][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x][y + 2] = 0;
            configuration.field[x][y + 3] = 0;
            configuration.field[x - 1][y + 3] = 0;
            break;
        case 8:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 2][y] = 0;
            configuration.field[x][y + 1] = 0;
            break;
        case 9:
            configuration.field[x][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x][y + 2] = 0;
            configuration.field[x + 1][y + 2] = 0;
            break;
        case 10:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 2][y] = 0;
            configuration.field[x + 2][y - 1] = 0;
            break;
        case 11:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 1][y + 1] = 0;
            configuration.field[x + 1][y + 2] = 0;
            break;
        case 12:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x + 2][y] = 0;
            configuration.field[x + 2][y + 1] = 0;
            break;
        case 13:
            configuration.field[x][y] = 0;
            configuration.field[x + 1][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x][y + 2] = 0;
            break;
        case 14:
            configuration.field[x][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x + 1][y + 1] = 0;
            configuration.field[x + 2][y + 1] = 0;
            break;
        case 15:
            configuration.field[x][y] = 0;
            configuration.field[x][y + 1] = 0;
            configuration.field[x][y + 2] = 0;
            configuration.field[x - 1][y + 2] = 0;
            break;

        case 16:
            configuration.field[x][y] = 0;
            break;
    }
}

/* Function which check founded price and compared it to the best one so far. */
void CSolver::solvePrice( CConfiguration & configuration ) {
    int price = configuration.l3Cnt * 2 + configuration.l4Cnt * 3 - 6 * (configuration.emptyTiles + configuration.imposibleTiles);
    //cout << price << " " << configuration.l3Cnt << " " << configuration.l4Cnt << " " << configuration.emptyTiles << " " << configuration.imposibleTiles << endl;
    if (price > this->bestPrice){

        #pragma omp critical
        {

                this->bestPrice = price;
                this->bestl3Cnt = configuration.l3Cnt;
                this->bestl4Cnt = configuration.l4Cnt;
                this->bestEmptyTiles = configuration.emptyTiles + configuration.imposibleTiles;
                for (int i = 0; i < this->N; i++) {
                    for (int j = 0; j < this->M; j++) {
                        this->bestSolution[i][j] = configuration.field[i][j];
                    }
                }

                /* Print details of new best solution. */
                cout << "New best price is " << price << " with " << configuration.l4Cnt << " L4, "
                     << configuration.l3Cnt
                     << " L3 and " << (configuration.emptyTiles + configuration.imposibleTiles) << " empty." << endl;
                cout << *(this);
        }

    }
}

/* Calculate actual price of placed L tiles and impossible tiles (not filled in current solution). */
int CSolver::evalActualPrice( int l3, int l4, int imp ){
    return (l3*2 + l4*3 - 6*imp);
}

/* Main solving function. Find first empty tile and tries to place a new L to it. */
void CSolver::solveStep( int x, int y, CConfiguration & configuration ){
    int shape = 0;
    bool flag = true;
    int possibleBestPrice = evalPol( configuration.emptyTiles );
    int actualPrice = evalActualPrice( configuration.l3Cnt, configuration.l4Cnt, configuration.imposibleTiles );
    if( (possibleBestPrice + actualPrice) <= bestPrice ) {
        return;
    }
    //cout << configuration << "\n";

    for( int i = x; i < this->N; i++){
        for( int j = y; j < this->M; j++ ){

            if( configuration.field[i][j] == 0 ){
                for( int k = 0; k < 16; k++) {
                    if (k >= 0 && k < 8) {
                        if (checkTiles(i, j, k, configuration)) {
                            flag = false;
                            fillTiles(i, j, configuration.id, k, configuration);
                            configuration.id++;
                            configuration.l4Cnt++;
                            configuration.emptyTiles = configuration.emptyTiles - 5;
                            solveStep(0, 0, configuration);
                            configuration.id--;
                            configuration.l4Cnt--;
                            configuration.emptyTiles = configuration.emptyTiles + 5;
                            restartTiles(i, j, k, configuration);
                        }
                    }
                    else {
                        if (checkTiles(i, j, k, configuration)) {
                            flag = false;
                            fillTiles(i, j, configuration.id, k, configuration);
                            configuration.id++;
                            configuration.l3Cnt++;
                            configuration.emptyTiles = configuration.emptyTiles - 4;
                            solveStep(0, 0, configuration);
                            configuration.id--;
                            configuration.l3Cnt--;
                            configuration.emptyTiles = configuration.emptyTiles + 4;
                            restartTiles(i, j, k, configuration);
                        }
                    }
                }

                /* Unfilled tile */
                if( flag ) {
                    fillTiles(i, j, configuration.id, 16, configuration);
                    configuration.imposibleTiles++;
                    configuration.emptyTiles--;
                    solveStep(0, 0, configuration);
                    configuration.imposibleTiles--;
                    configuration.emptyTiles++;
                    restartTiles(i, j, 16, configuration);
                }

                return;
            }

            else continue;
        }
    }

    //cout << configuration;
    cout  << configuration.l3Cnt << " " << configuration.l4Cnt << " " << configuration.emptyTiles << " " << configuration.imposibleTiles << endl;
    solvePrice( configuration );
}

/* Final print. */
void CSolver::printBestSolution() {
    cout << "--------------------------------------------------" << endl;
    cout << "Final price is " << this->bestPrice << " with " << this->bestl3Cnt << " L3, " << this->bestl4Cnt << " L4 and " << this->bestEmptyTiles << " empty." << endl;
    if( this->bestEmptyTiles ){
        cout << "Coordinates of empty tiles: " << endl;
        for( int i = 0; i < this->N; i++){
            for(int j = 0; j < this->M; j++){
                if( this->bestSolution[i][j] == -2 || this->bestSolution[i][j] == 0 )
                    cout << i << " " << j << endl;
            }
        }
    }
    cout << "Final grid: " << endl;
    for( int i = 0; i < this->N; i++){
        for(int j = 0; j < this->M; j++){
            cout << setw(2) << this->bestSolution[i][j] << " ";
        }
        cout << endl;
    }
    cout << "--------------------------------------------------" << endl;
}

/* Function which generates first solutions and add them to the queue. */
void CSolver::generateSolutions( int my_rank, int l3Cnt, int l4Cnt, int emptyTiles, int impossibleTiles, int id ) {
    for(int i = 0; i < this->N; i++) {
        for (int j = 0; j < this->M; j++) {
            if( this->field[i][j] == 0 ){
                for(int k = 0; k < 17; k++ ){
                    if( my_rank == 0 && ( this->solutionPool.size() >= NUM_CONFIGURATIONS*NUM_PROCS ) )
                        return;
                    else if( my_rank != 0 && ( this->solutionPool.size() >= NUM_CONFIGURATIONS*NUM_THREADS ) )
                        return;

                    CConfiguration * configuration;
                    if( my_rank == 0 )
                        configuration = new CConfiguration( this->field, this->bestl3Cnt, this->bestl4Cnt, 1, this->bestEmptyTiles, impossibleTiles );
                    else
                        configuration = new CConfiguration( this->field, l3Cnt, l4Cnt, id, emptyTiles, 0 );
                    if (k >= 0 && k < 8) {
                        if (checkTiles(i, j, k, *configuration)) {
                            fillTiles(i, j, configuration->id, k, *configuration);
                            configuration->id++;
                            configuration->l4Cnt++;
                            configuration->emptyTiles = configuration->emptyTiles - 5;
                            this->solutionPool.push_back(configuration);
                        }
                    }
                    else if( k >= 8 && k < 16 ){
                        if (checkTiles(i, j, k, *configuration)) {
                            fillTiles(i, j, configuration->id, k, *configuration);
                            configuration->id++;
                            configuration->l3Cnt++;
                            configuration->emptyTiles = configuration->emptyTiles - 4;
                            this->solutionPool.push_back(configuration);
                        }
                    }
                    else if( k == 16 ){
                        fillTiles(i, j, configuration->id, 16, *configuration);
                        configuration->imposibleTiles++;
                        configuration->emptyTiles--;
                        this->solutionPool.push_back(configuration);
                    }
                }
            }
        }
    }
}

/* Starting function. */
void CSolver::solve() {

    CConfiguration * actualConfiguration = nullptr;

    /* Parallel block */
    #pragma omp parallel private(actualConfiguration) /*shared( solutionPool, bestPrice, bestl3Cnt, bestl4Cnt, bestEmptyTiles, bestSolution )*/ num_threads (NUM_THREADS)
    {
        while( !solutionPool.empty() ) {
            #pragma omp critical
            {
                if( !solutionPool.empty() ){
                    //cout << *solutionPool.front();
                    actualConfiguration = solutionPool.front();
                    solutionPool.pop_front();
                    cout << "Solution taken out" << endl;
                }
            }
            solveStep(0, 0, *(actualConfiguration));
        }
    }

    printBestSolution();
}

/* Function for data parallelism. */
void CSolver::solveDataParallel() {

    CConfiguration * actualConfiguration = nullptr;
    int i;

    #pragma omp parallel for default ( shared ) private ( i, actualConfiguration ) num_threads (NUM_THREADS)
        for (i = 0; i < solutionPool.size(); i++) {
            actualConfiguration = solutionPool[i];
            cout << "Solution [" << i << "] started." << endl;
            solveStep(0, 0, *(actualConfiguration));
        }

    //printBestSolution();
}
