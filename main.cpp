#include "csolver.h"
#include <mpi.h>
//#include "../../School/openmpi-2.1.6/ompi/include/mpi.h"
#include <ctime>

#define TAG_WORK 0
#define TAG_FINISHED 1
#define TAG_RESULT 2
#define BUFFER_SIZE 250

using namespace std;

int main(int argc, char **argv) {
    int my_rank, p, N, M, bestPrice;

    /* MPI INIT */
    MPI_Init( &argc, &argv );
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    MPI_Comm_size(MPI_COMM_WORLD, &p);
    /* In case, there will be just master, do the OMP solution */
    if( p == 1 ){
        double start, end;

        start = MPI_Wtime();
        CSolver solver;
        solver.loadSolution( p );
        solver.generateSolutions(0, 0, 0, 0, 0, 1 );
        solver.solveDataParallel();
        solver.printBestSolution();
        end = MPI_Wtime();
        MPI_Finalize();
        cout << "Working time in ms: " << (int) ((end - start)*1000) << endl;
        return 0;
    }

    CSolver solver;

    /* initialization and load of the problem and generating of first branches. */
    if( my_rank == 0 ) {
        cout << "Number of processors: " << p << endl;
        solver.loadSolution(p);
        solver.generateSolutions( my_rank, 0, 0, 0, 0, 0);
        N = solver.getN(), M = solver.getM(), bestPrice = solver.getBestPrice();
    }

    /* Bcast of basic info. */
    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&M, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bestPrice, 1, MPI_INT, 0, MPI_COMM_WORLD);

    double start, end;
    clock_t startTime = clock();

    /* Master branch */
    if( my_rank == 0 ){
        start = MPI_Wtime();
        /* First send the work to the all slaves. */
        for( int i = 0, j = 1; i < solver.solutionPool.size() && j < p ; i++, j++ ){
            CConfiguration * configurationToSend;
            int * buffer = new int[BUFFER_SIZE];
            solver.fillBufferWithBestValues( buffer );
            configurationToSend = solver.solutionPool.front();
            solver.solutionPool.pop_front();
            configurationToSend->fillBufferToSend( buffer );
            MPI_Send(buffer, BUFFER_SIZE, MPI_INT, j, TAG_WORK, MPI_COMM_WORLD );
            delete [] buffer;
        }
        int workingSlaves = p - 1;

        int * buffer = new int[BUFFER_SIZE];
        /* Waiting for response. When it came, check if there is more solutions in the pool. If not kill the slave. */
        while( workingSlaves ){
            CConfiguration * configurationToSend;
            int * response = new int[BUFFER_SIZE];
            MPI_Status status;
            MPI_Recv(response, BUFFER_SIZE, MPI_INT, MPI_ANY_SOURCE, TAG_RESULT, MPI_COMM_WORLD, &status);
            CConfiguration configuration;
            configuration.loadDataFromBuffer( response, N, M );
            solver.solvePrice( configuration );
            delete [] response;

            int dest = status.MPI_SOURCE;
            if( solver.solutionPool.empty() ){
                MPI_Send(nullptr, 0, MPI_INT, dest, TAG_FINISHED, MPI_COMM_WORLD );
                workingSlaves--;
            } else{
                solver.fillBufferWithBestValues( buffer );
                configurationToSend = solver.solutionPool.front();
                solver.solutionPool.pop_front();
                configurationToSend->fillBufferToSend( buffer );
                MPI_Send(buffer, BUFFER_SIZE, MPI_INT, dest, TAG_WORK, MPI_COMM_WORLD );

                //delete configurationToSend;
            }
        }

        end = MPI_Wtime();
        cout << "Working time in ms: " << (int) ((end - start)*1000) << endl;

        delete[] buffer;
    }
    /* Slave branch. Slave receive some work, do it and send solution to the master. */
    else {
        int cntOfTask = 0;
        solver.setN(N);
        solver.setM(M);
        solver.setBestPrice(bestPrice);
        bool finished = false;
        int * buffer = new int[BUFFER_SIZE];
        int * result = new int[BUFFER_SIZE];
        while( ! finished ) {
            MPI_Status status;

            cout << "RECV " << my_rank << ", cnt of tasks: " << cntOfTask << endl;
            MPI_Recv(buffer, BUFFER_SIZE, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            cntOfTask++;
            /*for(int i = 0; i < N*M + 9; i++)
                cout << buffer[i] << " ";

            cout << endl;*/

            if (status.MPI_TAG == TAG_FINISHED){
                cout << "FINISHED " << my_rank << endl;
                finished = true;
                //delete [] buffer;
            }
            else{
                solver.loadDataFromBuffer( buffer );
                solver.generateSolutions( my_rank, buffer[5], buffer[6], buffer[7], buffer[8], buffer[9]);

                solver.solveDataParallel();


                solver.fillDataToBuffer( result );
                MPI_Send(result, BUFFER_SIZE, MPI_INT, 0, TAG_RESULT, MPI_COMM_WORLD );



            }

        }

        delete[] buffer;
        delete[] result;
    }
    clock_t stopTime = clock();

    float secsElapsed = (float)(stopTime  - startTime)/CLOCKS_PER_SEC;

    cout << "FINALIZE " << my_rank << " with time " << secsElapsed << endl;
    if( my_rank == 0 )
        solver.printBestSolution();

    /* shut down MPI */
    MPI_Finalize();
    return 0;
}