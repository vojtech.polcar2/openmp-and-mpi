//
// Created by trapper on 3.3.19.
//

#ifndef PDP_SEMESTRAL_CSOLVER_H
#define PDP_SEMESTRAL_CSOLVER_H

using namespace std;

#include "configuration.h"

#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <iomanip>
#include <omp.h>

/* Class for solving the POL problem. */
class CSolver{
    public:
        CSolver() {};
        ~CSolver() {};
        CSolver( const CSolver & csolver );
        void loadSolution( int p );
        void forbiddenTiles();
        void solve();
        void solveDataParallel();
        deque<CConfiguration* > solutionPool;
        void generateSolutions( int my_rank,  int l3Cnt, int l4Cnt, int emptyTiles, int impossibleTiles, int id  );
        int getBestPrice();
        int getN();
        int getM();
        int setN( int N );
        int setM( int M );
        int setBestPrice( int bestPrice );
        void fillBufferWithBestValues( int * buffer );
        void loadDataFromBuffer( int * buffer );
        void fillDataToBuffer( int * buffer );
        void solvePrice( CConfiguration & configuration );
        void printBestSolution();

    private:
        vector<vector<int>> field;
        vector<vector<int>> bestSolution;
        int bestPrice;
        int bestl3Cnt;
        int bestl4Cnt;
        int bestEmptyTiles;
        int N;
        int M;
        int Z;
        int NUM_PROCS;
        friend ostream & operator<<(ostream & os , const CSolver & obj);
        int evalPol( int number );
        void solveStep( int x, int y, CConfiguration & configuration );
        bool checkTiles( int x, int y, int shape , CConfiguration & configuration);
        void fillTiles( int x, int y, int id, int shape, CConfiguration & configuration );
        void restartTiles( int x, int y, int shape, CConfiguration & configuration );
        int evalActualPrice( int l3, int l4, int imp );
};

#endif //PDP_SEMESTRAL_CSOLVER_H
